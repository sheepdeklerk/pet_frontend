package pettadop.group.controllers;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import pettadop.group.LoginCurrentUserSetter;
import pettadop.group.models.Story;

@Controller
public class StoryController {

	//http://localhost:8080/stories
	
	Story[] stories;
	final String uri = "http://localhost:5000/allstories";
	RestTemplate restTemplate = new RestTemplate();

	@RequestMapping(value = "/stories")
	public ModelAndView Show()
	{
		ModelAndView model = new ModelAndView();
		
		//for logout modal
		model.addObject("User", LoginCurrentUserSetter.getAttributes());	
		model.setViewName("stories.jsp");

	
		return model;
	}


	@ModelAttribute("storyList")
    public Story[] getStories() {
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
	
		ResponseEntity<Story[]> responseEntity = restTemplate.exchange(uri, HttpMethod.GET, entity, Story[].class);

		stories = responseEntity.getBody();				
        return stories;
	}

}