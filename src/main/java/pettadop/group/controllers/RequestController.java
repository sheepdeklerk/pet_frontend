package pettadop.group.controllers;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import pettadop.group.LoginCurrentUserSetter;
import pettadop.group.models.Request;
import pettadop.group.models.Shelter;


@Controller
public class RequestController {
	
	
    final String uri = "http://localhost:5000/shelterrequests";
    final String uriShelter = "http://localhost:5000/shelter";

	Request[] requests;
    Shelter shelter;

    
	RestTemplate restTemplate = new RestTemplate();

	@RequestMapping("/requests")
	public ModelAndView Show()
	{	
		
		ModelAndView model = new ModelAndView();

		model.setViewName("request.jsp");	
		
		model.addObject("User", LoginCurrentUserSetter.getAttributes());	

		
		return model;
	}

	@ModelAttribute("requestList")
    public Request[] getRequests() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("shelter", LoginCurrentUserSetter.shelter.toString()); 
		HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
	
		ResponseEntity<Request[]> responseEntity = restTemplate.exchange(uri, HttpMethod.GET, entity, Request[].class);

		requests = responseEntity.getBody();				
        return requests;
    }
    
    @ModelAttribute("currentShelter")
	public Shelter getShelter()
	{
		HttpHeaders headers = new HttpHeaders();
		headers.set("shelter", LoginCurrentUserSetter.shelter.toString()); 
		HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
	
		ResponseEntity<Shelter> responseEntity = restTemplate.exchange(uriShelter, HttpMethod.GET, entity, Shelter.class);

		shelter = responseEntity.getBody();				
        return shelter;
	}
	


}