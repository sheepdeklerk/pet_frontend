package pettadop.group.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import pettadop.group.LoginCurrentUserSetter;

@Controller
public class AboutController {

	//http://localhost:8080/home
	//or http://localhost:8080/
	
	//Gifs used
	//Angry GIF by Jimmy the Bull
	@RequestMapping(value = {"/about"})

	//https://media.giphy.com/media/llmgz2cP4tsHK/giphy.gif
	//https://media.giphy.com/media/2ix6oI0fzvXfq/giphy.gif
	//https://media.giphy.com/media/l2JI304n1oBhOPYXu/giphy.gif
	//A diverse group of individuals dedicated to helping displaced animals find a home and be loved again.
	public ModelAndView Show()
	{
		ModelAndView model = new ModelAndView();
		List<String[]> arr = new ArrayList<>();
		arr.add(new String[]{"Brandon", "Ngwenya", "https://media.giphy.com/media/LqafmeaBVxCRG/giphy.gif", "I'm Dog Lover"});
		arr.add(new String[]{"Elizabeth", "van Staden", "https://media.giphy.com/media/llmgz2cP4tsHK/giphy.gif", "Cat Lover"});
		arr.add(new String[]{"Francois", "de Klerk", "https://media.giphy.com/media/l2JI304n1oBhOPYXu/giphy.gif", "Any Animal Lover"});
		arr.add(new String[]{"Potego", "Kgaphola ", "https://media.giphy.com/media/ZgN24y9eD8AEg/giphy.gif", "Any Animal Lover"});
		arr.add(new String[]{"Susan", "van Zyl", "https://media.giphy.com/media/snIsQ1UP3FuPC/giphy.gif", "Any Animal Lover"});
		arr.add(new String[]{"Tiaan", "Viljoen", "https://media.giphy.com/media/YldWUzIeHaNgY/giphy.gif", "Any Animal Lover"});
		
		model.addObject("Obs", arr);
		model.setViewName("about.jsp");

		//for logout modal
		model.addObject("User", LoginCurrentUserSetter.getAttributes());	
		return model;
	}
}