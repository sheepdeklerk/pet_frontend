package pettadop.group.controllers;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import pettadop.group.LoginCurrentUserSetter;
import pettadop.group.models.User;

@Controller
public class RegisterController {
    String uri = "http://localhost:5000/register";
    String uriTwo = "http://localhost:5000/getCities";

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public ModelAndView showForm() {
        ModelAndView model = new ModelAndView();
        model.setViewName("register.jsp");
        model.addObject("register", new User());

        model.addObject("User", LoginCurrentUserSetter.getAttributes());

        return model;
    }

    @RequestMapping("/registerfailed")
    public ModelAndView registerFail() {
        ModelAndView model = new ModelAndView();

        model.setViewName("registerfailed.jsp");

        return model;
    }

    @RequestMapping("/registersuccess")
    public ModelAndView registerSuccess() {
        ModelAndView model = new ModelAndView();

        model.setViewName("registersuccess.jsp");
        return model;
    }

    @RequestMapping(value = "/newuser", method = RequestMethod.GET)
    public String submit(@Valid @ModelAttribute("register") User register, BindingResult result, ModelMap model) {
        if (result.hasErrors()) {
            return "error";
        }
        // all new users should be created with no shelter id
        register.setShelter_id(new Long(-1));

        model.addAttribute("firstName", register.getFirstName());
        model.addAttribute("lastName", register.getLastName());
        model.addAttribute("cell", register.getCell());
        model.addAttribute("email", register.getEmail());
        model.addAttribute("password", register.getPassword());
        model.addAttribute("city_id", register.getCity_id());
        model.addAttribute("imageUrl", register.getImage());
        // model.addAttribute("shelter_id", register.getShelter_id());

        // System.out.println(register.getEmail());
        System.out.println("New USER");

        String bodyString = "{\"firstName\":\"" + register.firstName + "\"," + "\"lastName\":\"" + register.lastName
                + "\"," + "\"cell\":\"" + register.cell + "\"," + "\"email\":\"" + register.email + "\","
                + "\"imageUrl\":\"" + register.image + "\"," + "\"city_id\":\"" + register.city_id + "\"," +
                // "\"shelter_id\":\"" + register.shelter_id + "\"," +
                "\"password\":\"" + register.password + "\"}";

        // System.out.println(bodyString);

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<String> entity = new HttpEntity<String>(bodyString, headers);
        String message = restTemplate.postForObject(uri, entity, String.class);

        //System.out.println("/preference?email=" + register.email);

        if (message.equals("A new user has been registered."))
            return "/preference";
        else
            return "/registerfailed";

    }

    @ModelAttribute("cityList")
    public Map<Long, String> getCityList() {
        Map<Long, String> cityList = new HashMap<Long, String>();

        RestTemplate restTemp = new RestTemplate();
        HttpHeaders headersTwo = new HttpHeaders();

        HttpEntity<String> entityTwo = new HttpEntity<String>("", headersTwo);
        String current = restTemp.postForObject(uriTwo, entityTwo, String.class);

        String[] sList = current.split(";");

        for (String s : sList) {
            String[] arr = s.split(",");
            cityList.put(Long.parseLong(arr[0]), arr[1]);
        }
        return cityList;
    }

}
