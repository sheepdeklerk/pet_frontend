package pettadop.group.controllers;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import pettadop.group.LoginCurrentUserSetter;
import pettadop.group.models.Preference;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@Controller
public class PetPreferenceController {
    String uri = "http://localhost:5000/registerpreference";
    //String uriDogAge = "http://localhost:5000/getDogAge";
    String uriPetSize = "http://localhost:5000/petSizes";
    Long userId = null;
    Long currentPetTypeId = null;
    // this is a rest call dog

    @RequestMapping(value = "/addpreference", method = RequestMethod.GET)
        public String submit(@Valid @ModelAttribute("preference") Preference preference,
                         BindingResult result, ModelMap model) {
        if (result.hasErrors()) {
            return "error";
        }
        model.addAttribute("user_id", userId);
        model.addAttribute("catFriendly", preference.getCatFriendly());
        model.addAttribute("dogFriendly", preference.getDogFriendly());
        model.addAttribute("sex", preference.getSex());
        model.addAttribute("petType_id", currentPetTypeId);
        model.addAttribute("petSize_id", preference.getPetSize_id());

        String bodyString = "{\"user_id\":\"" + userId + "\"," +
                "\"catFriendly\":\"" + preference.catFriendly + "\"," +
                "\"dogFriendly\":\"" + preference.dogFriendly + "\"," +
                "\"sex\":\"" + preference.sex + "\"," +
                "\"petType_id\":\"" + currentPetTypeId + "\"," +
                "\"petSize_id\":\"" + preference.petSize_id + "\"}";


        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<String> entity = new HttpEntity<String>(bodyString, headers);
        String message = restTemplate.postForObject(uri, entity, String.class);

            if (message.equals("user reference created")) {
                return "/preferencesuccess";
            } 
            else
                 return "/error";

    }

    // @RequestMapping("/registerfailed")
    // public ModelAndView registerFail() {
    //     ModelAndView model = new ModelAndView();

    //     model.setViewName("registerfailed.jsp");

    //     return model;
    // }

    @RequestMapping("/preferencesuccess")
    public ModelAndView registerSuccess() {
        ModelAndView model = new ModelAndView();
        //for logout modal
        model.addObject("User", LoginCurrentUserSetter.getAttributes());
        model.setViewName("preferencesuccess.jsp");
        return model;
    }



    @ModelAttribute("options")
    public Map<Boolean, String> getOptions() {
        Map<Boolean,String> options = new HashMap<Boolean,String>();

       options.put(Boolean.TRUE, "Yes");
       options.put(Boolean.FALSE, "No");
        
        
       return options;
    }

    @ModelAttribute("petSizes")
    public Map<Long, String> getPetSizes() {
        Map<Long,String> petSizes = new HashMap<Long,String>();

        RestTemplate restTemp = new RestTemplate();
        HttpHeaders headersTwo = new HttpHeaders();

        
        HttpEntity<String> entityTwo = new HttpEntity<String>("", headersTwo);
        String current = restTemp.postForObject(uriPetSize , entityTwo, String.class);

        String[] sList = current.split(";");

        
        
        for (String s : sList) {
            String[] arr = s.split(",");
            petSizes.put(Long.parseLong(arr[0]), arr[1]);
        }
       return petSizes;
    }

    @RequestMapping(value="/petpreference", method= GET)
    public ModelAndView showForm(@RequestParam(value="user_id") String currentUserId,
                                 @RequestParam(value="petType_id") Long petType_id,
                                 RedirectAttributes attributes) {
        userId = Long.valueOf(currentUserId);
        currentPetTypeId = petType_id;
        attributes.addAttribute("user_id", currentUserId);
        attributes.addAttribute("petType_id", "1");
        return new ModelAndView("/petpreference.jsp", "preference", new Preference());
    }

}
