package pettadop.group.controllers;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import pettadop.group.LoginCurrentUserSetter;
import pettadop.group.PetCurrentSetter;
import pettadop.group.models.Pet;

@Controller
public class UserPreferenceController {

	final String uri = "http://localhost:5000/preferredpets";
	boolean firstLoad = true;

	int AmountPets = 0;

	String PrevCeiling = "hidden";
	String NextCeiling = "visible";

	Pet[] pets;
	RestTemplate restTemplate = new RestTemplate();
	Pet[] objects;
	private int GlobalPetId = 0;
	// http://localhost:8080/adoption

	HttpHeaders headers = new HttpHeaders();

	ModelAndView model = new ModelAndView();

	@RequestMapping("/preferred")
	public ModelAndView Show() {
		headers.set("id", LoginCurrentUserSetter.id.toString());
		HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);

		// ResponseEntity<Pets[]> responseEntity = restTemplate.getForEntity(uri,
		// Pets[].class);

		ResponseEntity<Pet[]> responseEntity = restTemplate.exchange(uri, HttpMethod.GET, entity, Pet[].class);

		objects = responseEntity.getBody();
		// MediaType contentType = responseEntity.getHeaders().getContentType();
		// HttpStatus statusCode = responseEntity.getStatusCode();

		if (objects.length > 0) {
			System.out.print(objects[0].petName);

			pets = objects;

			String initName = pets[GlobalPetId].petName;
			String initBreed = pets[GlobalPetId].breed;
			String initSex = pets[GlobalPetId].sex;
			String iniAge = Integer.toString(pets[GlobalPetId].age);
			String initStory = pets[GlobalPetId].story;
			// Long initLocation = pets[GlobalPetId].city;
			String initImagesrc = pets[GlobalPetId].imageUrlOne;
			String initImagesrcTwo = pets[GlobalPetId].imageUrlTwo;
			String initImagesrcThree = pets[GlobalPetId].imageUrlThree;
			boolean initAdopted = pets[GlobalPetId].adopted;
			String friendly = pets[GlobalPetId].getFriendly();


			PetCurrentSetter.Id = pets[GlobalPetId].id;

			String adoptedMessage = "";

			if (initAdopted == true) {
				adoptedMessage = "There are one or more people interested in this pet.";
			} else {
				adoptedMessage = "";
			}

			model.addObject("name", initName);
			model.addObject("breed", initBreed);
			model.addObject("sex", initSex);
			model.addObject("age", iniAge);
			model.addObject("story", initStory);
			// model.addObject("location", initLocation);
			model.addObject("imagesrc", initImagesrc);
			model.addObject("imagesrcTwo", initImagesrcTwo);
			model.addObject("imagesrcThree", initImagesrcThree);
			model.addObject("adoptedMessage", adoptedMessage);
			model.addObject("friendly", friendly);
			model.addObject("hiddenPrev", PrevCeiling);
			model.addObject("hiddenNext", NextCeiling);

			// for logout modal
			model.addObject("User", LoginCurrentUserSetter.getAttributes());

			AmountPets = pets.length;

			model.setViewName("adoption.jsp");

			return model;
		} else {
			ModelAndView model = new ModelAndView();
			model.addObject("title", "Sorry, no pets available.");
			model.addObject("description", "Unfortunately, no pets matched your preffered pet. Come back later, or while you are here, take a look around.");
			model.setViewName("empty-adoption.jsp");
			return model;
		}
	}

	public UserPreferenceController() {

	}

	@RequestMapping("/prefferednext")
	public String PostNext() {
		if (GlobalPetId < AmountPets - 1)
			GlobalPetId++;

		if (GlobalPetId == AmountPets - 1)
			NextCeiling = "hidden";
		else
			NextCeiling = "visible";

		if (GlobalPetId == 0)
			PrevCeiling = "hidden";
		else
			PrevCeiling = "visible";

		System.out.print("Print");

		return "/preferredpets";
	}

	@RequestMapping("/prefferedprevious")
	public String PostPrevious() {
		if (GlobalPetId > 0)
			GlobalPetId--;

		if (GlobalPetId == 0)
			PrevCeiling = "hidden";
		else
			PrevCeiling = "visible";

		if (GlobalPetId == AmountPets - 1)
			NextCeiling = "hidden";
		else
			NextCeiling = "visible";

		System.out.print("Print");

		return "/preferredpets";
	}

}