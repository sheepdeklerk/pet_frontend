package pettadop.group.controllers;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import pettadop.group.LoginCurrentUserSetter;
import pettadop.group.models.Pet;


@Controller
public class PetController {

    String uri = "http://localhost:5000/petregistration";
    String uriTypes = "http://localhost:5000/petTypes";
    String uriSizes = "http://localhost:5000/petSizes";

    @RequestMapping(value="/petregistration", method=RequestMethod.GET)
    public ModelAndView showForm() {
        ModelAndView model = new ModelAndView();

        model.addObject("pet", new Pet());
        
        //for logout modal
        model.addObject("User", LoginCurrentUserSetter.getAttributes());

        model.setViewName("pet.jsp");
        return model;
        
    }

    @RequestMapping(value = "/newpet", method = RequestMethod.GET)
    public String submit(@Valid @ModelAttribute("register")Pet pet, 
      BindingResult result, ModelMap model) {
        if (result.hasErrors()) {
            return "error";
        }
        model.addAttribute("petName", pet.getPetName());
        model.addAttribute("breed", pet.getBreed());
		model.addAttribute("sex", pet.getSex());
        model.addAttribute("age", pet.getAge());
        model.addAttribute("story", pet.getStory());
        model.addAttribute("imageUrlOne", pet.getImageUrlOne());
        model.addAttribute("catFriendly", pet.getCatFriendly());
        model.addAttribute("dogFriendly", pet.getDogFriendly());
        model.addAttribute("petType_id", pet.getPetType());
        model.addAttribute("petSize_id", pet.getPetSize());

        

        String bodyString = "{\"petName\":\"" + pet.petName + "\"," + 
                            "\"breed\":\"" + pet.breed + "\"," +
                            "\"sex\":\"" + pet.sex + "\"," +
                            "\"age\":\"" + pet.age + "\"," +
                            "\"story\":\"" + pet.story + "\"," +
                            "\"imageUrlOne\":\"" + pet.imageUrlOne + "\"," +
                            "\"catFriendly\":\"" + pet.catFriendly + "\"," +
                            "\"dogFriendly\":\"" + pet.dogFriendly + "\"," +
                            "\"petType_id\":\"" + pet.petType + "\"," +
                            "\"shelter_id\":\"" + LoginCurrentUserSetter.shelter + "\"," +
                            "\"petSize_id\":\"" + pet.petSize + "\"}";


        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<String> entity = new HttpEntity<String>(bodyString, headers);
        String message = restTemplate.postForObject(uri, entity, String.class);

        System.out.println(message);

        if (message.equals("A new user has been registered."))
            return "/petregistersuccess";
        else
            return "/petregisterfailure";
		
    }
    
    @RequestMapping("/petregistersuccess")
	public ModelAndView Show()
	{
        ModelAndView model = new ModelAndView();
        model.setViewName("petregistersuccess.jsp");
        
        //for logout modal
        model.addObject("User", LoginCurrentUserSetter.getAttributes());

		return model;
    }

    @RequestMapping("/petregisterfailure")
	public ModelAndView ShowFailure()
	{
		ModelAndView model = new ModelAndView();
        model.setViewName("petregisterfailure.jsp");
        
        //for logout modal
        model.addObject("User", LoginCurrentUserSetter.getAttributes());

		return model;
		
    }

    @ModelAttribute("options")
    public Map<Boolean, String> getOptions() {
        Map<Boolean,String> options = new HashMap<Boolean,String>();

       options.put(Boolean.TRUE, "Yes");
       options.put(Boolean.FALSE, "No");
        
        
       return options;
    }

    @ModelAttribute("petTypes")
    public Map<Long, String> getPetTypes() {
        Map<Long,String> petTypes = new HashMap<Long,String>();

        RestTemplate restTemp = new RestTemplate();
        HttpHeaders headersTwo = new HttpHeaders();

        
        HttpEntity<String> entityTwo = new HttpEntity<String>("", headersTwo);
        String current = restTemp.postForObject(uriTypes , entityTwo, String.class);

        String[] sList = current.split(";");

        
        
        for (String s : sList) {
            String[] arr = s.split(",");
            petTypes.put(Long.parseLong(arr[0]), arr[1]);
        }
       return petTypes;
    }

    @ModelAttribute("petSizes")
    public Map<Long, String> getPetSizes() {
        Map<Long,String> petSizes = new HashMap<Long,String>();

        RestTemplate restTemp = new RestTemplate();
        HttpHeaders headersTwo = new HttpHeaders();

        
        HttpEntity<String> entityTwo = new HttpEntity<String>("", headersTwo);
        String current = restTemp.postForObject(uriSizes , entityTwo, String.class);

        String[] sList = current.split(";");

        
        
        for (String s : sList) {
            String[] arr = s.split(",");
            petSizes.put(Long.parseLong(arr[0]), arr[1]);
        }
       return petSizes;
    }

    
}