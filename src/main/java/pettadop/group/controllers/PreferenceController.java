package pettadop.group.controllers;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import pettadop.group.LoginCurrentUserSetter;



@Controller
public class PreferenceController {


    @RequestMapping(value="/preference")
    public RedirectView showForm(@RequestParam(value="email") String userEmail,
                                 RedirectAttributes attributes) {
        String uri = "http://localhost:5000/getCurrentUserId";
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        String bodyString =  "{\"email\":\"" + userEmail + "\"}";
        HttpEntity<String> entity = new HttpEntity<String>(bodyString, headers);
        Long currentUserId = restTemplate.postForObject(uri, entity, Long.class);
        attributes.addAttribute("user_id", currentUserId);

         //for logout modal
         attributes.addAttribute("User", LoginCurrentUserSetter.getAttributes());
         
        return new RedirectView("preference.jsp");
    }
}
