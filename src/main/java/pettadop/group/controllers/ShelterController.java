package pettadop.group.controllers;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import pettadop.group.LoginCurrentUserSetter;
import pettadop.group.models.Shelter;


@Controller
public class ShelterController {

    String uri = "http://localhost:5000/shelterregistration";
    String uriCities = "http://localhost:5000/getCities";
    String uriShelters = "http://localhost:5000/getshelters";
    
    Shelter[] shelters;
	RestTemplate restTemplate = new RestTemplate();

    @RequestMapping(value = "/shelters")
    public ModelAndView shelters(){
        ModelAndView model = new ModelAndView();
        model.addObject("User", LoginCurrentUserSetter.getAttributes());	

        model.setViewName("shelter.jsp");
        return model;
    }

    @RequestMapping(value="/shelterregistration", method=RequestMethod.GET)
    public ModelAndView showForm() {

        ModelAndView model = new ModelAndView();
        model.setViewName("add-shelter.jsp");
        model.addObject("shelter", new Shelter());
         //for logout modal
         model.addObject("User", LoginCurrentUserSetter.getAttributes());
        return model;
    }

    @RequestMapping(value = "/newshelter", method = RequestMethod.GET)
    public String submit(@Valid @ModelAttribute("shelter")Shelter shelter, 
      BindingResult result, ModelMap model) {
        if (result.hasErrors()) {
            return "error";
        }
        
        model.addAttribute("name", shelter.getName());
        model.addAttribute("description", shelter.getDescription());
		model.addAttribute("email", shelter.getEmail());
        model.addAttribute("tel", shelter.getTel());
        model.addAttribute("website", shelter.getWebsite());
        model.addAttribute("logo", shelter.getLogo());
        model.addAttribute("city_id", shelter.getCity_id());

        

        String bodyString = "{\"name\":\"" + shelter.name + "\"," + 
                            "\"description\":\"" + shelter.description + "\"," +
                            "\"email\":\"" + shelter.email + "\"," +
                            "\"tel\":\"" + shelter.tel + "\"," +
                            "\"website\":\"" + shelter.website + "\"," +
                            "\"logo\":\"" + shelter.logo + "\"," +
                            "\"city_id\":\"" + shelter.city_id + "\"}";

        //System.out.println(bodyString);

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<String> entity = new HttpEntity<String>(bodyString, headers);
        String message = restTemplate.postForObject(uri, entity, String.class);

        System.out.println(message);

        if (message.equals("A new shelter has been registered."))
            return "/admin";
        else
            return "/registerfailed";
		
    }
    
    @ModelAttribute("cityList")
    public Map<Long, String> getCityList() {
        Map<Long,String> cityList = new HashMap<Long,String>();

        RestTemplate restTemp = new RestTemplate();
        HttpHeaders headersTwo = new HttpHeaders();

        
        HttpEntity<String> entityTwo = new HttpEntity<String>("", headersTwo);
        String current = restTemp.postForObject(uriCities , entityTwo, String.class);

        String[] sList = current.split(";");

        
        
        for (String s : sList) {
            String[] arr = s.split(",");
            cityList.put(Long.parseLong(arr[0]), arr[1]);
        }
       return cityList;
    }
    
    @ModelAttribute("shelterList")
    public Shelter[] getShelters(){

        HttpHeaders headers = new HttpHeaders();
		HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
	
		ResponseEntity<Shelter[]> responseEntity = restTemplate.exchange(uriShelters, HttpMethod.GET, entity, Shelter[].class);

		shelters = responseEntity.getBody();				
        return shelters;
    }


}