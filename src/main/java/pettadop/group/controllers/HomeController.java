package pettadop.group.controllers;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import pettadop.group.Login;
import pettadop.group.LoginCurrentUserSetter;
import pettadop.group.models.Shelter;
import pettadop.group.models.Story;

@Controller
public class HomeController {

	//http://localhost:8080/home
	//or http://localhost:8080/
	
	//Gifs used
	//Angry GIF by Jimmy the Bull
	
	Story[] stories;
	final String uri = "http://localhost:5000/recentstories";
	RestTemplate restTemplate = new RestTemplate();
	String uriShelters = "http://localhost:5000/getshelters";
    Shelter[] shelters;

	@RequestMapping(value = {"/home","/"})
	public ModelAndView Show()
	{
		ModelAndView model = new ModelAndView();
		
		//for logout modal
		model.addObject("User", LoginCurrentUserSetter.getAttributes());
		

		if(LoginCurrentUserSetter.id == null){
			model.setViewName("home.jsp");
		}
		else{
			if(LoginCurrentUserSetter.userType==1){
				return new ModelAndView("redirect:" + "/admin");
			}
			if(LoginCurrentUserSetter.shelter == null){
				return new ModelAndView("redirect:" + "/adoption");
			}
			else{
				return new ModelAndView("redirect:" + "/shelterpets");
			}
		}

		if (LoginCurrentUserSetter.email == null)
		{
			
		}
		else
		{
			
		}
	
		return model;
	}

	@RequestMapping(value = {"/modal"})
	public ModelAndView Login()
	{
		return new ModelAndView("modal.jsp", "login", new Login());
	}

	@ModelAttribute("storyList")
    public Story[] getStories() {
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
	
		ResponseEntity<Story[]> responseEntity = restTemplate.exchange(uri, HttpMethod.GET, entity, Story[].class);

		stories = responseEntity.getBody();				
        return stories;
	}

	@ModelAttribute("shelterList")
    public Shelter[] getShelters(){

        HttpHeaders headers = new HttpHeaders();
		HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
	
		ResponseEntity<Shelter[]> responseEntity = restTemplate.exchange(uriShelters, HttpMethod.GET, entity, Shelter[].class);

		shelters = responseEntity.getBody();				
        return shelters;
    }

}