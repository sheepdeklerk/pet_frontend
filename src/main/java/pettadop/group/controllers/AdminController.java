package pettadop.group.controllers;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import pettadop.group.LoginCurrentUserSetter;
import pettadop.group.models.Shelter;
import pettadop.group.models.ShelterUser;
import pettadop.group.models.User;

@Controller
public class AdminController {

    // http://localhost:8080/admin

    // Gifs used
    // Angry GIF by Jimmy the Bull

    final String uri = "http://localhost:5000/getjoinedusers";
    final String uriTwo = "http://localhost:5000/getjoinedshelters";
    final String uriShelterUser = "http://localhost:5000/usershelter";

    User[] users;
    Shelter[] shelters;
    RestTemplate restTemplate = new RestTemplate();

    @RequestMapping(value = { "/admin" })
    // only admin users
    public ModelAndView Show() {
        ModelAndView model = new ModelAndView();
        model.addObject("User", LoginCurrentUserSetter.getAttributes());
        model.setViewName("admin.jsp");

        if (LoginCurrentUserSetter.id == null) {
            return new ModelAndView("redirect: /error");
        } else {
            if (LoginCurrentUserSetter.userType != 1) {
                return new ModelAndView("redirect: /error");
            }
        }
        // for logout modal

        return model;
    }

    @RequestMapping(value = { "/shelterusers" })
    public ModelAndView ShelterUser() {
        ModelAndView model = new ModelAndView();
        model.addObject("User", LoginCurrentUserSetter.getAttributes());
        model.setViewName("shelter-user.jsp");
        model.addObject("shelter_user", new ShelterUser());
        if (users.length > 0) {
            Map<Long, String> userNameList = new HashMap<Long, String>();
            for (User user : users) {
                userNameList.put(user.id, user.getFullName());
            }

            model.addObject("userNameList", userNameList);
        }
        if (shelters.length > 0) {
            Map<Long, String> shelterNameList = new HashMap<Long, String>();
            for (Shelter shelter : shelters) {
                shelterNameList.put(shelter.id, shelter.name);
            }

            model.addObject("shelterNameList", shelterNameList);
        }
        // for logout modal

        return model;
        // return new ModelAndView("shelter_user.jsp", "user", new User());
    }

    @RequestMapping(value = "/updateshelteruser", method = RequestMethod.GET)
    public String submit(@Valid @ModelAttribute("shelter_user") ShelterUser shelterUser, BindingResult result,
            ModelMap model) {
        if (result.hasErrors()) {
            return "error";
        }
        // all new users should be created with no shelter id
        model.addAttribute("user_id", shelterUser.getUser_id());
        model.addAttribute("shelter_id", shelterUser.getShelter_id());

        // System.out.println(register.getEmail());
        // System.out.println(register.getPassword());

        String bodyString = "{\"user_id\":\"" + shelterUser.user_id + "\"," + "\"shelter_id\":\""
                + shelterUser.shelter_id + "\"}";

        // System.out.println(bodyString);

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<String> entity = new HttpEntity<String>(bodyString, headers);
        String message = restTemplate.postForObject(uriShelterUser, entity, String.class);

        System.out.println(message);

        if (message.equals("User has been updated."))
            return "/admin";
        else
            return "/admin";

    }

    @ModelAttribute("userList")
    public User[] getPets() {

        if (LoginCurrentUserSetter.userType != null) {

            HttpHeaders headers = new HttpHeaders();
            headers.set("userType", LoginCurrentUserSetter.userType.toString());
            HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);

            ResponseEntity<User[]> responseEntity = restTemplate.exchange(uri, HttpMethod.GET, entity, User[].class);

            users = responseEntity.getBody();
        }

        return users;
    }

    @ModelAttribute("shelterList")
    public Shelter[] getShelters() {

        if (LoginCurrentUserSetter.userType != null) {

            HttpHeaders headers = new HttpHeaders();
            headers.set("userType", LoginCurrentUserSetter.userType.toString());
            HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);

            ResponseEntity<Shelter[]> responseEntity = restTemplate.exchange(uriTwo, HttpMethod.GET, entity,
                    Shelter[].class);

            shelters = responseEntity.getBody();
        }

        return shelters;
    }

}