package pettadop.group.controllers;


import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import pettadop.group.LoginCurrentUserSetter;
import pettadop.group.models.Pet;
import pettadop.group.models.Shelter;


@Controller
public class ShelterAdoptionController {
	
	
	final String uri = "http://localhost:5000/shelterpets";
	final String uriShelter = "http://localhost:5000/shelter";
	Pet[] pets;
	Shelter shelter;
	RestTemplate restTemplate = new RestTemplate();

	
	
		
	
	
	@RequestMapping("/shelterpets")
	public ModelAndView Show()
	{	
		
		ModelAndView model = new ModelAndView();

		model.setViewName("shelteradoption.jsp");	
		
		model.addObject("User", LoginCurrentUserSetter.getAttributes());	

		
		return model;
	}

	@ModelAttribute("petList")
    public Pet[] getPets() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("shelter", LoginCurrentUserSetter.shelter.toString()); 
		HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
	
		ResponseEntity<Pet[]> responseEntity = restTemplate.exchange(uri, HttpMethod.GET, entity, Pet[].class);

		pets = responseEntity.getBody();				
        return pets;
	}
	
	@ModelAttribute("currentShelter")
	public Shelter getShelter()
	{
		
		
			HttpHeaders headers = new HttpHeaders();
		 
			// if(LoginCurrentUserSetter.shelter!=null){
			// 	
			// }
			headers.set("shelter", LoginCurrentUserSetter.shelter.toString()); 
			HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
			ResponseEntity<Shelter> responseEntity = restTemplate.exchange(uriShelter, HttpMethod.GET, entity, Shelter.class);
			shelter = responseEntity.getBody();		
			return shelter;		
		
	
	}
	
	

	public ShelterAdoptionController()
	{

	}
	


}