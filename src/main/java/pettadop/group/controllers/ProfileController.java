package pettadop.group.controllers;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import pettadop.group.LoginCurrentUserSetter;
import pettadop.group.LoginTokenSetter;
import pettadop.group.models.User;

@Controller
public class ProfileController {
    RestTemplate restTemplate = new RestTemplate();

    HttpHeaders headers = new HttpHeaders();

    ModelAndView model = new ModelAndView();
    String uri = "http://localhost:5000/updateuser";
    String uricities = "http://localhost:5000/getCities";
    String uriget = "http://localhost:5000/getUser";

    @RequestMapping(value="/profile", method=RequestMethod.GET)
    public ModelAndView showForm() {
        if (LoginCurrentUserSetter.email.isEmpty())
        {
            model.setViewName("login.jsp");	
		    return model;
        }

        HttpHeaders headersTwo = new HttpHeaders();
        headersTwo.setContentType(MediaType.APPLICATION_JSON);
        headersTwo.set("token", LoginTokenSetter.token); 
        headersTwo.set("id", LoginCurrentUserSetter.id.toString()); 

        // String body = "{"+ "\"" +"email"+ "\"" +":"+"\"" + LoginCurrentUserSetter.email + "\"" + "," + 
        // 		"\"" +"cell"+ "\"" +":"+"\"" + LoginCurrentUserSetter.cell + "\"" +"}";
        
        HttpEntity<String> entityTwo = new HttpEntity<String>("", headersTwo);
        User userForm = restTemplate.postForObject(uriget , entityTwo, User.class);


        // String[] sList = current.split(",");
        // User userForm = new User();
        // userForm.cell = sList[0];
        // userForm.firstName = sList[1];
        // userForm.lastName=sList[2];
        // userForm.email=sList[3];
        // userForm.image = sList[4];
        // userForm.password = sList[5];
        // System.out.println("------------------------------------------------------------------");
        // System.out.println(sList[2]);
        //			return user.cell +","+ user.firstName + "," + user.lastName+ "," + user.email+ "," + user.shelter_id + "," +user.city_id;
        ModelAndView profilemodel = new ModelAndView("profile.jsp", "profile", userForm);
		profilemodel.addObject("User", LoginCurrentUserSetter.getAttributes());	
        
        profilemodel.addObject("userForm", userForm);
        return profilemodel;
    }

    
    @RequestMapping(value = "/edituser", method = RequestMethod.GET)
    public String submit(@Valid @ModelAttribute("profile")User profile, 
      BindingResult result, ModelMap model) {
        if (result.hasErrors()) {
            return "error";
        }
        profile.setShelter_id(new Long(-1));

        model.addAttribute("firstName", profile.getFirstName());
        model.addAttribute("lastName", profile.getLastName());
		model.addAttribute("cell", profile.getCell());
      //  model.addAttribute("email", profile.getEmail());
      //  model.addAttribute("password", profile.getPassword());
      //  model.addAttribute("city_id", profile.getCity_id());
        model.addAttribute("imageUrl", profile.getImage());
      //  model.addAttribute("shelter_id", profile.getShelter_id());

        //System.out.println(register.getEmail());
        //System.out.println(register.getPassword());

        String bodyString = "{\"firstName\":\"" + profile.firstName + "\"," + 
                            "\"lastName\":\"" + profile.lastName + "\"," +
                            "\"cell\":\"" + profile.cell + "\"," +
                            //"\"email\":\"" + profile.email + "\"," +
                            //"\"city_id\":\"" + profile.city_id + "\"," +
                            //"\"shelter_id\":\"" + profile.shelter_id + "\"," +
                            //"\"password\":\"" + profile.password + "\"," +

                            "\"imageUrl\":\"" + profile.image + "\"}";

        System.out.println(bodyString);

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("id", LoginCurrentUserSetter.id.toString()); 

        HttpEntity<String> entity = new HttpEntity<String>(bodyString, headers);
        String message = restTemplate.postForObject(uri, entity, String.class);

        System.out.println(message);

        if (message.equals("User Updated"))
        {
            //LoginCurrentUserSetter.email = profile.email;
            //LoginCurrentUserSetter.firstName = profile.firstName;
            //LoginCurrentUserSetter.lastName = profile.lastName;
            return "/profileupdatesuccess";
        }
        else
            return "/profileupdatefailure";
		
    }

    @RequestMapping("/profileupdatesuccess")
	public ModelAndView Show()
	{
        model.setViewName("profileupdatesuccess.jsp");	
		 //for logout modal
         model.addObject("User", LoginCurrentUserSetter.getAttributes());
		return model;
    }

    @RequestMapping("/profileupdatefailure")
	public ModelAndView ShowFailure()
	{
        model.setViewName("profileupdatefailure.jsp");	
		 //for logout modal
         model.addObject("User", LoginCurrentUserSetter.getAttributes());
		return model;
    }

    @ModelAttribute("cityList")
    public Map<Long, String> getCityList() {
        Map<Long,String> cityList = new HashMap<Long,String>();

        RestTemplate restTemp = new RestTemplate();
        HttpHeaders headersTwo = new HttpHeaders();

        
        HttpEntity<String> entityTwo = new HttpEntity<String>("", headersTwo);
        String current = restTemp.postForObject(uricities , entityTwo, String.class);

        String[] sList = current.split(";");

        
        
        for (String s : sList) {
            String[] arr = s.split(",");
            cityList.put(Long.parseLong(arr[0]), arr[1]);
        }
       return cityList;
    }
    
}