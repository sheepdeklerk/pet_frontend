package pettadop.group.controllers;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import pettadop.group.LoginCurrentUserSetter;
import pettadop.group.models.User;

@Controller
public class TestController {

	//http://localhost:8080/test
	
	
	final String uri = "http://localhost:5000/test";
	RestTemplate restTemplate = new RestTemplate();

	@RequestMapping(value = "/test")
	public ModelAndView Show()
	{
		ModelAndView model = new ModelAndView();
        
        HttpHeaders headers = new HttpHeaders();
		HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
	
		ResponseEntity<User[]> responseEntity = restTemplate.exchange(uri, HttpMethod.GET, entity, User[].class);
        User[] users = responseEntity.getBody();				
                
		//for logout modal
		model.addObject("User", LoginCurrentUserSetter.getAttributes());	
        model.setViewName("stories.jsp");
        model.addObject("stories", users);

	
		return model;
	}

}