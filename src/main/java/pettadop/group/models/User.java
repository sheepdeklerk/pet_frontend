package pettadop.group.models;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class User {
    
    public Long id;
    public String lastName;
    public String firstName;
    public String cell;
    public String email;
    public String password;
    public Long city_id;
    public String image;
    public Long shelter_id ; 
    public Long userType_id;
    public String prefDogAge;
    public boolean dogFriendly;
    public boolean catFriendly;
    public String prefBreed;
    public String prefGender;
    public Long petTypeId;
    public String petSize;

    //display purposes
    public String type;
    public String city;
    public String shelter;

    public String getFullName()
    {
        return this.firstName + " " + this.lastName;
    }

   public Long getId() {
       return id;
   }

   public void setId(Long id) {
       this.id = id;
   }

   public String getLastName() {
       return lastName;
   }

   public void setLastName(String lastName) {
       this.lastName = lastName;
   }

   public String getFirstName() {
       return firstName;
   }

   public void setFirstName(String firstName) {
       this.firstName = firstName;
   }

   public String getCell() {
       return cell;
   }

   public void setCell(String cell) {
       this.cell = cell;
   }

   public String getEmail() {
       return email;
   }

   public void setEmail(String email) {
       this.email = email;
   }

   public String getPassword() {
       return password;
   }

   public void setPassword(String password) {
       this.password = password;
   }

   public  Long getCity_id() {
       return this.city_id;
   }

   public void setCity_id( Long city_id) {
       this.city_id= city_id;
   }
   public String getImage() {
       return image;
   }

   public void setImage(String image) {
       this.image = image;
   }

   public Long getShelter_id() {
       return shelter_id;
   }

   public void setShelter_id(Long shelter_id) {
       this.shelter_id = shelter_id;
   }

   public Long getUserType_id() {
       return userType_id;
   }

   public void setUserType_id(Long userType_id) {
       this.userType_id = userType_id;
   }


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getShelter() {
        return shelter;
    }

        
}       