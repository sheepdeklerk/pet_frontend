package pettadop.group.models;

public class Dog {
   public Long PetTypeId;

   public String dogAge;

   public boolean catFriendly;

   public boolean dogFriendly;

   public String breed;

   public String gender;


    public String getDogAge() {
        return dogAge;
    }

    public void setDogAge(String dogAge) {
        this.dogAge = dogAge;
    }

    public boolean isCatFriendly() {
        return catFriendly;
    }

    public void setCatFriendly(boolean catFriendly) {
        this.catFriendly = catFriendly;
    }

    public boolean isDogFriendly() {
        return dogFriendly;
    }

    public void setDogFriendly(boolean dogFriendly) {
        this.dogFriendly = dogFriendly;
    }

    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Long getPetTypeId() {
        return PetTypeId;
    }

    public void setPetTypeId(Long petTypeId) {
        PetTypeId = petTypeId;
    }
}
