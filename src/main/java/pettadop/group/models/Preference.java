package pettadop.group.models;

public class Preference {
  
    
    public Long id;
    public Long user_id;
    public Long petType_id;
    public Long petSize_id;
    public String sex;
    public Boolean catFriendly;
    public Boolean dogFriendly;
   

    public Preference()
    {

    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    
    public Long getUser_id()
    {
        return this.user_id;
    }

    public void setUser_id(Long user_id)
    {
        this.user_id = user_id;
    }


    
    public Long getPetType_id()
    {
        return this.petType_id;
    }

    public void setPetType_id(Long petType_id)
    {
        this.petType_id = petType_id;
    }

    public Long getPetSize_id()
    {
        return this.petSize_id;
    }

    public void setPetSize_id(Long petSize_id)
    {
        this.petSize_id = petSize_id;
    }

    
    public String getSex()
    {
        return this.sex;
    }

    public void setSex(String sex)
    {
        this.sex = sex;
    }

    public Boolean getCatFriendly()
    {
        return this.catFriendly;
    }

    public void setCatFriendly(Boolean catFriendly)
    {
        this.catFriendly = catFriendly;
    }

    
    public Boolean getDogFriendly()
    {
        return this.dogFriendly;
    }

    public void setDogFriendly(Boolean dogFriendly)
    {
        this.dogFriendly = dogFriendly;
    }


}