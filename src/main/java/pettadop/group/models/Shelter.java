package pettadop.group.models;

public class Shelter {
  
   
	public Long id;
    public String name;
    public String description;
    public String email;
    public String tel;
    public String website;
    public String logo;
    public Long city_id;
    public String city;

    
    public String getName()
    {
        return this.name;
    }
    public void setName(String name)
    {
        this.name = name;;
    }
    public String getDescription()
    {
        return this.description;
    }
    public void setDescription(String description)
    {
        this.description = description;
    }
    public String getTel()
    {
        return this.tel;
    }
    public void setTel(String tel)
    {
        this.tel = tel;;
    }

    public String getEmail()
    {
        return this.email;
    }
    public void setEmail(String email)
    {
        this.email = email;;
    }

    public String getWebsite()
    {
        return this.website;
    }
    public void setWebsite(String website)
    {
        this.website = website;;
    }

    public String getLogo()
    {
        return this.logo;
    }
    public void setLogo(String logo)
    {
        this.logo = logo;;
    }

    public Long getCity_id()
    {
        return this.city_id;
    }
    public void setCity_id(Long city)
    {
        this.city_id = city;
    }

    public String getCity()
    {
        return this.city;
    }
    public void setCity(String city)
    {
        this.city = city;
    }
}