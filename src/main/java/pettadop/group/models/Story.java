package pettadop.group.models;

public class Story{
    public Long id;
    public Long shelter_id;
    public String title;
    public String story;
    public String imageUrlOne;
    public Long city_id;
    public String author;

    public Long getShelter(){
        return this.shelter_id;
    }
    public String getTitle(){
        return this.title;
    }
    public String getStory(){
        return this.story;
    }
    public String getImageUrlOne(){
        return this.imageUrlOne;
    }
    public Long getCity(){
        return this.city_id;
    }
    public String getAuthor(){
        return this.author;
    }
    public void setShelter(Long shelter_id){
        this.shelter_id = shelter_id;
    }
    public void setTitle(String title){
        this.title = title;
    }
    public void setStory(String story){
        this.story = story;
    }
    public void setImageUrlOne(String imageUrlOne){
        this.imageUrlOne = imageUrlOne;
    }
    public void setCity(Long city_id){
        this.city_id = city_id;
    }
    public void setAuthor(String author){
        this.author = author;
    }


}
