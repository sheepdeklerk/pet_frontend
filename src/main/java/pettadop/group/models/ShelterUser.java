package pettadop.group.models;

public class ShelterUser {
	public Long user_id;
    public Long shelter_id;


    public Long getUser_id()    {
        return user_id;
    }
    public void setUser_id(Long user_id)    {
        this.user_id =  user_id;
    }
    public Long getShelter_id()    {
        return shelter_id;
    }
    public void setShelter_id(Long shelter_id)    {
        this.shelter_id = shelter_id;
    }
}