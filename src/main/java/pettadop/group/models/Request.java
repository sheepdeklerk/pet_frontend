package pettadop.group.models;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Request {
  
   
	public Long id;
    public Long user_id;
    public Long shelter_id;
    public Long pet_id;
    public String userName;
    public String userCell;
    public String userEmail;
    public String petName;
    public Date date;
    public String message;


    
    public Long getId()
    {
        return this.id;
    }
    public void setId(Long id)
    {
        this.id = id;;
    }
    public Long getUser_id()
    {
        return this.user_id;
    }
    public void setUser_id(Long user_id)
    {
        this.user_id = user_id;;
    }

    public Long getShelter_id()
    {
        return this.shelter_id;
    }
    public void setDescription(Long shelter_id)
    {
        this.shelter_id = shelter_id;
    }
    public Long getPet_id()
    {
        return this.pet_id;
    }
    public void setPet_id(Long pet_id)
    {
        this.pet_id = pet_id;;
    }
    public String getUserName()
    {
        return this.userName;
    }
    public void setUserName(String userName)
    {
        this.userName = userName;;
    }

    public String getUserCell()
    {
        return this.userCell;
    }
    public void setUserCell(String userCell)
    {
        this.userCell = userCell;;
    }

    public String getUserEmail()
    {
        return this.userEmail;
    }
    public void setUserEmail(String userEmail)
    {
        this.userEmail = userEmail;
    }

    public String getPetName()
    {
        return this.petName;
    }
    public void setPetName(String petName)
    {
        this.petName = petName;
    }

    public String getdate()
    {
        SimpleDateFormat formatter = new SimpleDateFormat("dd MMMM yyyy");  
        return formatter.format(this.date);  
    }
    public void setDate(Date date)
    {
        this.date = date;
    }

    public String getMessage()
    {
        return this.message;
    }
    public void setMessage(String message)
    {
        this.message = message;
    }




}