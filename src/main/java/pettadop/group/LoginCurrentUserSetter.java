package pettadop.group;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

@Component
public class LoginCurrentUserSetter {
	public static String cell;
	public static String firstName;
	public static String lastName;
	public static String email;
	public static Long shelter;
	public static Long id;
	public static Long userType;
	
	public static void logout()
	{
		LoginCurrentUserSetter.cell = null;
		LoginCurrentUserSetter.firstName = null;
		LoginCurrentUserSetter.lastName = null;
		LoginCurrentUserSetter.email = null;
		LoginCurrentUserSetter.shelter = null;
		LoginCurrentUserSetter.id = null;
		LoginCurrentUserSetter.userType = null;
	}

	public static Map<String,String> getAttributes()
	{
		Map<String,String> attributes = new HashMap<String,String>();
		if(firstName != null)
		{
		attributes.put("name", firstName + " " + lastName);

		if(userType==1){
			attributes.put("type", "admin");
		}
		else
		{
			if(shelter!=null)
				attributes.put("type", "shelter");
			else
				attributes.put("type", "normal");
		}
		
		

		return attributes;
	}
		return null;
	}
}
