<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="container">

    <nav class="navbar navbar-expand-sm navbar-toggleable-sm navbar-light box-shadow topnav">
        <img src="/images/logo_text.png" width="180" />
                
                    <!-- <img
                        src="https://images.squarespace-cdn.com/content/v1/53d838c5e4b0d47dc0878ce2/1485389537301-550N4JSS7J29D90E7HYA/ke17ZwdGBToddI8pDm48kPZik6YZgxQ3JNnHIKB1FiVZw-zPPgdn4jUwVcJE1ZvWQUxwkmyExglNqGp0IvTJZamWLI2zvYWH8K3-s_4yszcp2ryTI0HqTOaaUohrI8PIe6uTlgGZWg3p7giFZu5065eRoyGjsue_OYafdtpYqlU/PF+Foundation+Logo_V3.png"
                        alt="Cinque Terre" width="190" height="70"> -->
                
                


        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target=".navbar-collapse"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="navbar-collapse collapse d-sm-inline-flex flex-sm-row-reverse">
            <ul class="navbar-nav flex-grow-1">
                 <c:choose>
                    <c:when test="${User!=null}">

                        <c:choose>
                            <c:when test="${User.type!='admin'}">
                                <!-- <li class="nav-item">
                                    <a class="nav-link text-muted pt-2" href="/adoption">Adoption</a>
                                </li> -->
                                <c:choose>
                                    <c:when test="${User.type=='normal'}">
                                        <li class="nav-item">
                                            <a class="nav-link text-muted pt-2" href="/adoption">Adoption</a>
                                            <a class="nav-link text-muted pt-2" href="/preferred">Pets for me</a>
                                        </li>
                                    </c:when>
                                    <c:otherwise>
                                        <li class="nav-item">
                                            <a class="nav-link text-muted pt-2" href="/shelterpets">Pets</a>
                                        </li>
                                    </c:otherwise>
                                </c:choose>
                            </c:when>
                            <c:otherwise>
                                <li class="nav-item">
                                    <a class="nav-link text-muted pt-2" href="/admin">Admin</a>
                                </li>
                            </c:otherwise>
                        </c:choose>
                    </c:when>
                </c:choose>
                <c:choose>

                
                    <c:when test="${User==null}">

                                    
                    <!-- User  NOT Logged In -->
                    <li class="nav-item">
                        <a class="nav-link text-muted pt-2" href="/home">Home</a>
                    </li>
                    
                    <li class="nav-item">
                        <a class="nav-link text-muted pt-2" href="/login">Log In</a>
                    </li>
                    </c:when>
                    </c:choose>
                    <li class="nav-item">
                        <a class="nav-link text-muted pt-2" href="/shelters">Shelters</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-muted pt-2" href="/about">About</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-muted pt-2" href="/stories">Stories</a>
                    </li>


            </ul>

        </div>

        <c:choose>
            <c:when test="${User!=null}">

                <p class="navbar-text">
                    Welcome ${User.name}
                    <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRqf84eCVGQWKp-qErPk0njWhqragEhnKOom1UNJY0hcOBb3vgX0g&s"
                        href="/profile" class="img-responsive" height="40" width="40">
                    <br />
                    <!-- <a href="#">Log Out</a> -->
                    <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#myModal">Log out</button>
                    <a class="btn btn-outline-primary" href="/profile" role="button">Profile</a>

                </p>
            </c:when>
        </c:choose>
    </nav>
</div>

<jsp:include page="logoutmodal.jsp" />
