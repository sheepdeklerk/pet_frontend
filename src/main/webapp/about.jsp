<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="ISO-8859-1">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
    integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">
  <link rel="stylesheet" href="/css/style.css">

  <title>About - Pet Tinder</title>
</head>

<body>
  <jsp:include page="header.jsp" />
 <div class="container border-bottom border-top pb-2 mb-3">
      <h1 class="display-4 text-center">About us</h1>
</div>
  <div class="container">
    <div class="jumbotron">

      <div class="row">
        <p class="text-body">Pet Tinder is a rescue portal dedicated to helping displaced animals find a home and be
          loved again and to give you a platform to help you connect and
          match with your dream pet. Search multiple rescue organisations to find the right pet for your home.</p>
      </div>

      </div>
      <div class="row">
        <div class="container border-bottom pb-2 mb-3">
          <h1 class="display-4 text-center">Meet the team</h1>
        </div>
        <br>
      </div>
      <div class="container">
        <div class="jumbotron">
        <div class="row">

          <%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
          <c:forEach var="per" items="${Obs}">

            <div class="col-sm">
              <div class="card" style="width: 18rem; margin-bottom: 2rem;">
                <img class="card-img-top" src="${per[2]}" alt="Card image cap">
                <div class="card-body">
                  <h5 class="card-title">${per[0]} ${per[1]}</h5>
                  <p class="card-text">${per[3]}.</p>
                  <a href="#" class="btn btn-dark">Contact me</a>
                </div>
              </div>
            </div>

          </c:forEach>
        </div>

      </div>
    </div>
  </div>
  <div class="container">
    <jsp:include page="footer.jsp" />

  </div>

  <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
    integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
    crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
    integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
    crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
    integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
    crossorigin="anonymous"></script>
  <script>
    $('.carousel').carousel().carousel('cycle');
  </script>
</body>

</html>