 <!-- Modal -->  
  <div class="modal fade" id="myModal" role="dialog">  
    <div class="modal-dialog">  
      
      <!-- Modal content-->  
      <div class="modal-content">  
        <div class="modal-header">  
          <button type="button" class="close" data-dismiss="modal">
            <i class="fa fa-close"></i>
          </button>  
          <h4 class="modal-title">Are you sure you want to log out?</h4>  
        </div>  
 
        <div class="modal-footer">  
          <a  class="btn btn-pet" href="/logout" role="button">Yes</a>

          <button type="button" class="btn btn-dark" data-dismiss="modal">No</button>  
        </div>  
      </div>  
        
    </div>  
  </div>  
    

