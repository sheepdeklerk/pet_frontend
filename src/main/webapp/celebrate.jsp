<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="ISO-8859-1">
    <title>Celebrate!</title>
    <!-- Required meta tags -->
    <meta charset="ISO-8859-1">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">
    <link rel="stylesheet" href="/css/style.css">
    <style>
        .aligncenter {
            text-align: center;
        }
    </style>
</head>

<body>
    <jsp:include page="header.jsp" />
    <div class="container border-bottom border-top pb-2 mb-3">
         <h1 class="display-4 text-center">Congratulations</h1>
   </div>
      <div class="container">

        <h1 class="display-2 animated swing" style="text-align:center">Well done ${currentUserName}!
        </h1>
        <br>

        <h3 class="text-center" style="text-align:center">Confirmation will be sent to ${currentUserNumber}</h3>
        <br>

        <h4>A message will also be sent to the shelter and they will set up a meeting soon!</h4>
        <p class="aligncenter">
            <img src="https://media1.tenor.com/images/15ba033ccc4f29967bb00290b541bda4/tenor.gif?itemid=12928945">
        </p>

        <a class="btn-lg btn btn-dark btn-block" href="/adoption">Go back</a>
    </div>
    <div class="container">
        <jsp:include page="footer.jsp" />
    
      </div>
</body>

</html>