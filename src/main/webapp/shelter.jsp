<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="ISO-8859-1">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
    integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">
  <link rel="stylesheet" href="/css/style.css">

  <title>Shelters - Pet Tinder</title>
</head>

<body>
  <jsp:include page="header.jsp" />
  <div class="container border-bottom border-top pb-2 mb-3">
       <h1 class="display-4 text-center">Meet our partners</h1>
  </div>
  <div class="container">
    <div class="container">

      <div class="card-group">
        <c:forEach items="${shelterList}" var="shelterList">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">${shelterList.name} (${shelterList.city})</h3>
            </div>
            <div class="card-body text-center">
              <div style="height: 120px;">
                <img class="img-thumbnail" src="${shelterList.logo}" alt="${shelterList.name} logo" style="max-height: 7rem;">
              </div>

              <p class="card-text">${shelterList.description}</p>

            </div>

            <p class="card-footer">
              <i class="fa fa-envelope-o pr-3"></i><a href="mailtp:${shelterList.email}">${shelterList.email}</a>
              <br />
              <i class="fa fa-phone pr-3"></i>${shelterList.tel}
              <br />
              <i class="fa fa fa-laptop pr-3"></i><a href="${shelterList.website}">${shelterList.website}</a>

          </div>
        </c:forEach>
      </div>


    </div>
    <div class="container pt-3">
      <div class="jumbotron jumbotron-fluid">
        <div class="container">
          <h1 class="display-4">Want to register your shelter?</h1>
          <p class="lead">Send an email to <a href="mailto:info@pettinder.co.za">info@pettinder.co.za</a> to find out
            how.
          </p>
        </div>
      </div>
    </div>
  </div>
  </div>
  <div class="container">
    <jsp:include page="footer.jsp" />

  </div>

  <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
    integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
    crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
    integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
    crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
    integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
    crossorigin="anonymous"></script>
</body>

</html>