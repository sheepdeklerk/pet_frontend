<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>

<head>
	<meta charset="ISO-8859-1">
	<title>Pet Tinder - Adoption</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
		integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">



	<link rel="stylesheet" href="/css/style.css">

	<style>
		.carousel {
			border-radius: 10px 10px 10px 10px;
			overflow: hidden;
		}

		.carousel-inner>.item>img,
		.carousel-inner>.item>a>img {
			width: 100%;
			margin: auto;
		}

		.swal2-popup {
			font-size: 1.6rem !important;
		}

		body {
			font-family: "Open Sans", -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", Helvetica, Arial, sans-serif;
		}
	</style>

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">


</head>

<body>

	<jsp:include page="header.jsp" />
    <div class="container border-bottom border-top pb-2 mb-3">
         <h1 class="display-4 text-center">Meet our pets</h1>
   </div>

	<div class="container">
		<br>

		<div class="jumbotron container box-shadow" style="min-height: 520px;">
			<div class="row">
				<div class="col-sm">
					<h1 class="display-4 bounceIn">${name}</h1>

					<div id="myCarousel" class="carousel slide">
						<!-- Indicators -->
						<ol class="carousel-indicators">
							<li class="item1 active"></li>
							<li class="item2"></li>
							<li class="item3"></li>
						</ol>

						<!-- Wrapper for slides -->
						<div class="carousel-inner" role="listbox" style="width: 100%; height: 250px !important;">

							<div class="item active">
								<img height="40" width="40" src="${imagesrc}">

							</div>

							<div class="item">
								<img height="40" width="40" src="${imagesrcTwo}">
							</div>

							<div class="item">
								<img height="40" width="40" src="${imagesrcThree}">
							</div>



						</div>

						<!-- Left and right controls -->
						<a class="left carousel-control" href="#myCarousel" role="button">
							<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						</a> <a class="right carousel-control" href="#myCarousel" role="button"> <span
								class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						</a>
					</div>
				</div>

				
				<div class="col-sm">

					
					<br>
					<br>
					<br>
					<br>

					<h3 class="text-secondary">Breed: ${breed}</h3>
					<h3 class="text-secondary">Sex: ${sex}</h3>
					<h3 class="text-secondary">Age: ${age} years</h3>
					<br> <a class="btn btn-meet text-white" onclick="myAlert()">Meet this pet</a>
				</div>

				<div class="col-sm">
					<br>
					<br>
					<br>
					<br>
					<h3 class="text-secondary">My story</h3>
					<p class="text-muted">${story}</p>
					<br>
					<p class="text-danger">${adoptedMessage}</p>


				</div>

			</div>
			<h4 class="text-danger text-center">${friendly}</h3>
		</div>

		<div class="container">
			<div class="row">
				<div class="col-sm">

					<a class="btn btn-pet btn-lg btn-block text-white" href="/previous"
						style="visibility: ${hiddenPrev};">Previous</a>
				</div>
				<div class="col-sm">
					<a class="btn btn-dark btn-lg btn-block" href="/next" style="visibility: ${hiddenNext};">Next</a>




				</div>
			</div>
		</div>

		<br>
		<br>

		<div class="container">
			<jsp:include page="footer.jsp" />
		
		  </div>

	</div>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
	<script>
		// function myAlert() {
		// 	Swal.fire({
		// 		title: 'Please confirm',
		// 		text: "Are you sure about the adoption?",
		// 		icon: 'question',
		// 		showCancelButton: true,
		// 		confirmButtonColor: '#3085d6',
		// 		cancelButtonColor: '#d33',
		// 		confirmButtonText: 'Yes, I\'m sure!',
		// 		showClass: {
		// 			popup: 'animated fadeInDown faster'
		// 		},
		// 		hideClass: {
		// 			popup: 'animated fadeOutUp faster'
		// 		}
		// 	}).then((result) => {
		// 		if (result.value) {
		// 			goToCelebrate()
		// 		}
		// 	})
		// }

		function myAlert() {
			Swal.fire({
				title: 'Meet this pet',
				//text: 'Message to shelter (optional)',
				input: 'textarea',
				inputPlaceholder: 'Type your message here...',
				inputAttributes: {
					'aria-label': 'Type your message here'
				},
				showCancelButton: true,
				confirmButtonText: 'Submit Request',
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				showClass: {
					popup: 'animated fadeInDown faster'
				},
				hideClass: {
					popup: 'animated fadeOutUp faster'
				}
			}).then((result) => {
				 if (result.value) {
					//goToCelebrate()
					goToRequested(result.value);
					goToCelebrate();
				 }
			})
		}

		function goToRequested(message){
			jQuery.get('requested?message='+message, 'text');
		}



		function goToCelebrate() {
			window.open("/celebrate", "_top");
			
		}

		$(document).ready(function () {
			// Activate Carousel
			$("#myCarousel").carousel();

			// Enable Carousel Indicators
			$(".item1").click(function () {
				$("#myCarousel").carousel(0);
			});
			$(".item2").click(function () {
				$("#myCarousel").carousel(1);
			});
			$(".item3").click(function () {
				$("#myCarousel").carousel(2);
			});

			// Enable Carousel Controls
			$(".left").click(function () {
				$("#myCarousel").carousel("prev");
			});
			$(".right").click(function () {
				$("#myCarousel").carousel("next");
			});
		});
	</script>


</body>

</html>