<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="ISO-8859-1">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">
    <link rel="stylesheet" href="/css/style.css">

    <title>Home - Pet Tinder</title>
</head>

<body>
    <jsp:include page="header.jsp" />
  <div class="container border-bottom border-top pb-2 mb-3">
       <h1 class="display-4 text-center">Register a new pet</h1>
 </div>
    <div class="container">
        <div class="row">
            <div class="col-sm">
                <img src="https://media1.tenor.com/images/c78b85dae254923d75bee4c59a1ed501/tenor.gif?itemid=12928949">

            </div>
            <div class="container p-3 my-3 border col-sm">
                <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
                <form:form method="GET" action="/newpet" modelAttribute="pet" class="form">
                    <tr>
                        <td>
                            Name:
                            <form:input path="petName" type="text" placeholder="Daisy" class="form-control"
                                required="required" />
                        </td>
                    </tr>
                    <br />

                    <tr>
                        <td>
                            Breed:
                            <form:input path="breed" type="text" placeholder="Jack Russel" class="form-control"
                                required="required" />
                        </td>
                    </tr>
                    <br />

                    <tr>
                        <td>
                            Sex:
                            <form:select path="sex" class="dropdown form-control">
                                <form:option value="Male" label="Male" class="dropdown-item" />
                                <form:option value="Female" label="Female" class="dropdown-item" />
                            </form:select>
                        </td>
                    </tr>
                    <br />
                    <tr>
                        <td>
                            Cat Friendly:                              
                            <form:select path="catFriendly" items="${options}" class="form-control" required="required" />
                        </td>
                    </tr>
                    <br />
                    <tr>
                        <td>
                            Dog Friendly:
                            <form:select path="dogFriendly" items="${options}" class="form-control" required="required" />

                        </td>
                    </tr>
                    <br />
                    <tr>
                        <td>
                            Age:
                            <form:input path="age" type="Number" placeholder="3" class="form-control"
                                required="required" />
                        </td>
                    </tr>
                    <br />

                    <tr>
                        <td>
                            Story
                            <form:textarea path="story" rows="5" cols="30" placeholder="Daisy is the sweetest dog you will ever meet! With the cutest little tail that never stops wagging she will steal your heart." class="form-control"
                                required="required" />
                        </td>
                    </tr>
                    <br />

                    <tr>
                        <td>
                            Image URL:
                            <form:input path="imageUrlOne" type="text" placeholder="Image" class="form-control"
                                required="required" />
                        </td>
                    </tr>
                    <br />
                    <tr>

                        <td>
                            Type:
                            <form:select path="petType" items="${petTypes}" class="form-control" required="required" /> 
                        </td>
                    </tr>
                    <br />
                    <tr>

                        <td>
                            Size:
                            <form:select path="petType" items="${petSizes}" class="form-control" required="required" />
                        </td>
                    </tr>
                    <br />

                    <tr>
                        <br />
                        <td>
                            <input type="submit" value="Add Pet" class="btn-lg btn btn-dark btn-block" />
                        </td>
                    </tr>


                </form:form>
            </div>

        </div>
    </div>
    <div class="container">
        <jsp:include page="footer.jsp" />
    
      </div>

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
        crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
        crossorigin="anonymous"></script>
</body>

</html>