<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="ISO-8859-1">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
    integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">
  <link rel="stylesheet" href="/css/style.css">

  <title>Admin - Pet Tinder</title>
</head>

<body>
  <jsp:include page="header.jsp" />
  <div class="container border-bottom border-top pb-2 mb-3">
    <h1 class="display-4 text-center">Admin</h1>
  </div>
  <div class="container">
    <div class="row">
      <h1>Users</h1>
      <table class="table">
        <tr>
          <th>First Name</th>
          <th>Last Name</th>
          <th>Cellphone</th>
          <th>Email</th>
          <th>City</th>
          <th>Shelter</th>
          <th>User Type</th>
        </tr>

        <c:forEach items="${userList}" var="userList">
          <tr>
            <td>${userList.firstName}</td>
            <td>${userList.lastName}</td>
            <td>${userList.cell}</td>
            <td>${userList.email}</td>
            <td>${userList.city}</td>
            <td>${userList.shelter}</td>
            <td>${userList.type}</td>
          </tr>
        </c:forEach>
      </table>

      <a class="btn-lg btn btn-dark btn-block" href="/shelterusers">Add a shelter user</a>

      <br>
      <hr/>
      <br/>
      <h1>Shelters</h1>

          <div class="container">

      <div class="card-group">
        <c:forEach items="${shelterList}" var="shelterList">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">${shelterList.name}</h3>
              <h3 class="card-title">${shelterList.city} </h3>
            </div>
            <div class="card-body text-center">
              <div style="height: 120px;">
                <img class="img-thumbnail" src="${shelterList.logo}" alt="${shelterList.name} logo">
              </div>

              <p class="card-text">${shelterList.description}</p>

            </div>

            <p class="card-footer">
              <i class="fa fa-envelope-o pr-3"></i><a href="mailto:${shelterList.email}">${shelterList.email}</a>
              <br />
              <i class="fa fa-phone pr-3"></i>${shelterList.tel}
              <br />
              <i class="fa fa fa-laptop pr-3"></i><a href="${shelterList.website}">${shelterList.website}</a>

          </div>
        </c:forEach>
      </div>


    </div>
      <a class="btn-lg btn btn-dark btn-block" href="/shelterregistration">Add a shelter</a>

    </div>
  </div>
  <div class="container">
    <jsp:include page="footer.jsp" />

  </div>

  <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
    integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
    crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
    integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
    crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
    integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
    crossorigin="anonymous"></script>
</body>

</html>