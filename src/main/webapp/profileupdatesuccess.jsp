<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="ISO-8859-1">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
    integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">
  <link rel="stylesheet" href="/css/style.css">

  <title>Profile Updated Successfully - Pet Tinder</title>
</head>

<body>
  <jsp:include page="header.jsp" />
  <div class="container border-bottom border-top pb-2 mb-3">
       <h1 class="display-4 text-center">Profile Update Success</h1>
 </div>

      <div class="container">
        <div class="row">
          <div class="col-sm">
            <img src="https://media.giphy.com/media/9r2Yflv0PDievgiKRi/giphy.gif">

          </div>
          <div class="container p-3 my-3 border col-sm">

            <p class="lead"> </p>
            <p class="lead">You have Successfully updated your profile! Yaay</p>
            <br>

            <!-- <a class="btn btn-lg btn-dark btn-block" href="/profile" role="button">Go to Profile page</a> -->
            <!-- <br> -->
            <a class="btn btn-lg btn-dark btn-block" href="/home" role="button">Back to Home page</a>
          </div>
        </div>
      </div>

      <div class="container">
        <jsp:include page="footer.jsp" />
    
      </div>

  <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
    integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
    crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
    integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
    crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
    integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
    crossorigin="anonymous"></script>
</body>

</html>