<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="ISO-8859-1">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
    integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="/css/style.css">

  <title>Home - Pet Tinder</title>
</head>

<body>
 <jsp:include page="header.jsp" />
 <div class="container border-bottom border-top pb-2 mb-3">
      <h1 class="display-4 text-center">Welcome to Pet Tinder</h1>
</div>
  <div class="container">
    <div class="row">
      <!-- carousel -->
      <div id="homeCarousel" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#homeCarousel" data-slide-to="0" class="active"></li>
          <li data-target="#homeCarousel" data-slide-to="1"></li>
          <li data-target="#homeCarousel" data-slide-to="2"></li>
          <li data-target="#homeCarousel" data-slide-to="3"></li>
        </ol>
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img src="/images/home_item_1.png" class="d-block w-100" alt="">

          </div>
          <div class="carousel-item">
            <img src="/images/home_item_2.png" class="d-block w-100" alt="">
            <div class="carousel-caption text-right">
              <a class="btn btn-success" href="/login" role="button">Log in</a>

              <p>Making a difference starts here.</p>
            </div>
          </div>
          <div class="carousel-item">
            <img src="/images/home_item_3.png" class="d-block w-100" alt="">
            <div class="carousel-caption text-center">
              <a class="btn btn-light" href="/about" role="button">About Us</a>
              <p>Find out more about Pet Tinder.</p>
            </div>
          </div>
          <div class="carousel-item">
            <img src="/images/home_item_4.png" class="d-block w-100" alt="">
            <div class="carousel-caption text-right">
              <a class="btn btn-warning" href="#" role="button">Adoption Stories</a>
              <p>Read the success stories of people around the country.</p>
            </div>
          </div>
        </div>
        <a class="carousel-control-prev" href="#homeCarousel" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#homeCarousel" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    </div>

    <div class="row bg-light">
      <div class="col-md-6">
        <h2 class="">
          FORGET <span class="display-4">SHOPPING</span>, <br />
          START <span class="display-4">ADOPTING</span>
        </h2>
        <p>
          Pet Tinder was created to assist shelters accross South Africa responsibly rehome the animals in their care.
        </p>
        <p><a href="/about">Read more...</a></p>
      </div>
      <div class="col-md-3 pt-4">
        <h3>START SWIPING</h3>
        <p>A furry friend is always to be found... Too start your journey on finding the perfect companion log in now.
        </p>
        <a class="btn btn-dark mt-3 mb-3" href="/login" role="button">Login</a>
        <p class="small pt-2"><em>dont have an account yet?</em> <a href="/register">click here...</a></p>
      </div>
      <div class="col-md-3 pt-4">
        <h3>SUCCESS STORIES</h3>
        <p>Read the successes from accross the country.</p>
        <a class="btn btn-pet mt-3 mb-3" href="#" role="button">Adoption Tales</a>
        <p class="small pt-2"><em>want to tell your story?</em> <a href="#">click here...</a></p>
      </div>
    </div>
  <div class="row">
    <h1 class="display-3">
      Adoption success stories
    </h1>
  </div>
    <div class="row">
      
      <!-- <div class="card-deck">
        <c:forEach items="${storyList}" var="storyList">
          <div class="card">
            <img class="card-img-top" src="${storyList.imageUrlOne}" alt="Card image cap">
            <div class="card-body">
              <h5 class="card-title">${storyList.title}</h5>
              <p class="card-text ">${storyList.story}</p>
            </div>
            <div class="card-footer">
              <small class="text-muted"> - ${storyList.author}</small>

            </div>
          </div>
        </c:forEach>
      </div> -->

      <c:forEach items="${storyList}" var="storyList">

            <div class="col-lg">
              <div class="card" >
                <img class="card-img-top" src="${storyList.imageUrlOne}" alt="${storyList.title}">
                <div class="card-body">
                  <h5 class="card-title">${storyList.title}</h5>
                  <p class="card-text">${storyList.story}</p>
                </div>
                <div class="card-footer">
                  <small class="text-muted"> - ${storyList.author}</small>
    
                </div>
              </div>
            </div>

          </c:forEach>

    </div>
    <div class="container">
      <div class="jumbotron">

      <div class="row">
        <div class="display-4 text-center" style="margin-bottom: 2rem;">Our partner shelters</div>
      </div>  
      <div class="row">
        <c:forEach items="${shelterList}" var="shelterList">
          <div class="card" style="width: 8rem; margin-right: 1rem;">
            <img href="${shelterList.website}" class="card-img-top mw-100" src="${shelterList.logo}" alt="Card image cap">
            <h5 class="card-title">${shelterList.name}</h5>
          </div>
        </c:forEach>
      </div>
      </div>
    </div>

  </div>
  <div class="container">
    <jsp:include page="footer.jsp" />

  </div>

  <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
    integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
    crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
    integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
    crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
    integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
    crossorigin="anonymous"></script>
</body>

</html>