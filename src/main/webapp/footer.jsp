<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!-- Footer -->
<div class="row font-small blue mt-4 border-top bg-light mb-3 p-3">

    <!-- Footer Links -->
    <div class="container-fluid text-center text-lg-left">

        <!-- Grid row -->
        <div class="row">

            <!-- Grid column -->
            <div class="col-lg-3 mt-md-0 mt-3">

                <!-- Content -->
                <h5 class="text-uppercase mb-3">Pet Tinder</h5>
                <img class="" style="width: 180px; height: 180px;" src="/images/logo.png" />

            </div>

            <div class="col-lg-3 mt-md-0 mt-3">
                <h5 class="text-uppercase mb-3">Contact Us</h5>
                <!-- Content -->
                <i class="fa fa-envelope-o pr-3"></i><a href="mailto:adoption@pettinder.co.za">info@pettinder.co.za</a>
                <br />
                <i class="fa fa-phone pr-3"></i>021 450 2584
                <br />

            </div>
            <!-- Grid column -->

            <hr class="clearfix w-100 d-md-none pb-3">

            <!-- Grid column -->
            <div class="col-lg-3 mb-md-0 mb-3">

                <!-- Links -->
                <h5 class="text-uppercase mb-3">Quick Links</h5>

                <ul class="list-unstyled">
                    <li>
                        <a href="/stories">Adoption Stories</a>
                    </li>
                    <li>
                        <a href="/shelters">Shelters</a>
                    </li>
                    <c:choose>
                        <c:when test="${User==null}">
                            <li>
                                <a href="/login">Log In</a>
                            </li>
                            <li>
                                <a href="/register">Register</a>
                            </li>
                        </c:when>
                    </c:choose>
                </ul>

            </div>
            <!-- Grid column -->

            <!-- Grid column -->
            <div class="col-lg-3 mb-md-0 mb-3">

                <!-- Links -->
                <h5 class="text-uppercase mb-3">Similar Initiatives</h5>

                <ul class="list-unstyled">
                    <li>
                        <a href="https://www.pawsofcapetown.com/">Paws of Cape Town</a>
                    </li>
                    <li>
                        <a href="http://darg.org.za/">Domestic Animal Rescue Group</a>
                    </li>

                </ul>

            </div>
            <!-- Grid column -->

        </div>
        <!-- Grid row -->
        <div class="text-center">
            <span class="fa fa-copyright"></span> Copyright:
            <a href="#"> PetTinder.com</a>
        </div>
    </div>
    <!-- Footer Links -->

    <!-- Copyright -->

</div>
<!-- Copyright -->


<!-- Footer -->