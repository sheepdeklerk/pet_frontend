<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="ISO-8859-1">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
    integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">
  <link rel="stylesheet" href="/css/style.css">

    <title>Add Shelter - Pet Tinder</title>
</head>

<body>
    <jsp:include page="header.jsp" />
    <div class="container border-bottom border-top pb-2 mb-3">
         <h1 class="display-4 text-center">Register a new shelter</h1>
   </div>
    <div class="container">
        <div class="row">
            <div class="col-sm">
                <img src="https://media1.tenor.com/images/c78b85dae254923d75bee4c59a1ed501/tenor.gif?itemid=12928949">

            </div>
            <div class="container p-3 my-3 border col-sm">
                <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
                <form:form method="GET" action="/newshelter" modelAttribute="shelter" class="form">
                    <tr>
                        <td>
                            Name:
                            <form:input path="name" type="text" placeholder="Name" class="form-control"
                                required="required" />
                        </td>
                    </tr>
                    <br />

                    <tr>
                        <td>
                            Description:
                            <form:input path="description" type="text" placeholder="Description" class="form-control"
                                required="required" />
                        </td>
                    </tr>
                    <br />
                    <tr>
                        <td>
                            Email:
                            <form:input path="email" type="text" placeholder="Email" class="form-control"
                                required="required" />
                        </td>
                    </tr>
                    <br />
                    <tr>
                        <td>
                            Telephone:
                            <form:input path="tel" type="text" placeholder="Telephone Number" class="form-control"
                                required="required" />
                        </td>
                    </tr>
                    <br />
                    <tr>
                        <td>
                            Website:
                            <form:input path="website" type="text" placeholder="Website" class="form-control"
                                required="required" />
                        </td>
                    </tr>
                    <br />
                    <tr>
                        <td>
                            Logo:
                            <form:input path="logo" type="text" placeholder="Logo" class="form-control"
                                required="required" />
                        </td>
                    </tr>
                    <br />
                    <tr>
                        <td>
                            City:
                            <form:select path="city_id" items="${cityList}" class="form-control" required="required" />
                        </td>
                    </tr>
                    <br />
                    

                    <tr>
                        <br />
                        <td>
                            <input type="submit" value="Add Shelter" class="btn btn-dark btn-block btn-lg" />
                            <a class="btn btn-pet btn-block btn-lg" href="/admin" role="button">Cancel</a>
                        </td>
                        <td>
                            
                        </td>
                    </tr>


                </form:form>

                
               
            </div>

        </div>
    </div>
    <div class="container">
        <jsp:include page="footer.jsp" />
    
      </div>

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
        crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
        crossorigin="anonymous"></script>
</body>

</html>