<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html lang="en"><head>
    <!-- Required meta tags -->
    <meta charset="ISO-8859-1">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">
    <link rel="stylesheet" href="/css/style.css">
    <style>
        #rig {
            max-width:900px;
            margin:0 auto;
            padding:0;
            font-size:0;
            list-style:none;
            background-color:#000;
        }
        #rig li {
            display: inline-block;
            *display:inline;/*for IE6 - IE7*/
            width:25%;
            vertical-align:middle;
            box-sizing:border-box;
            margin:0;
            padding:0;
        }

        /* The wrapper for each item */
        .rig-cell {
            /*margin:12px;
            box-shadow:0 0 6px rgba(0,0,0,0.3);*/
            display:block;
            position: relative;
            overflow:hidden;
            margin: 10px;
        }

        /* If have the image layer */
        .rig-img {
            display:block;
            width: 100%;
            height: 150px;
            border:none;
            transform:scale(1);
            transition:all 1s;
        }

        #rig li:hover .rig-img {
            transform:scale(1.05);
        }

        /* If have the overlay layer */
        .rig-overlay {
            position: absolute;
            display:block;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
            margin: auto;
            background: #3DC0F1 url(img/link.png) no-repeat center 20%;
            background-size:50px 50px;
            opacity:0;
            filter:alpha(opacity=0);/*For IE6 - IE8*/
            transition:all 0.6s;
        }
        #rig li:hover .rig-overlay {
            opacity:0.8;
        }

        /* If have captions */
        .rig-text {
            display:block;
            padding:0 30px;
            box-sizing:border-box;
            position:absolute;
            left:0;
            width:100%;
            text-align:center;
            text-transform:capitalize;
            font-size:18px;
            font-weight:bold;
            font-family: 'Oswald', sans-serif;
            font-weight:normal!important;
            top:40%;
            color:white;
            opacity:0;
            filter:alpha(opacity=0);/*For older IE*/
            transform:translateY(-20px);
            transition:all .3s;
        }
        #rig li:hover .rig-text {
            transform:translateY(0px);
            opacity:0.9;
        }

        @media (max-width: 9000px) {
            #rig li {
                width:33%;
            }
        }

        @media (max-width: 700px) {
            #rig li {
                width:33.33%;
            }
        }

        @media (max-width: 550px) {
            #rig li {
                width:50%;
            }
        }
    </style>
    <title>Home - Pet Tinder</title>
</head>

<body>
<div class="row">
    <jsp:include page="header.jsp" />
</div>
<div class="container">
    <div class="row">
        <div class="container p-3 my-3 border col-sm">
            <h1 class="display-4">Select your preferred companion</h1>
            <br>

            <form:form id="register">

                <ul id="rig">
                    <li>
                        <a class="rig-cell" onclick="window.location.href='/petpreference' + window.location.search + '&petType_id=1'">
                            <img class="rig-img" src="https://media2.s-nbcnews.com/j/newscms/2018_20/1339477/puppy-cute-today-180515-main_a936531048fdb698635dd1b418abdee9.fit-760w.jpg">
                            <span class="rig-overlay"></span>
                            <span class="rig-text">Dogs</span>
                        </a>
                    </li>
                    <li>
                        <a class="rig-cell" onclick="window.location.href='/petpreference' + window.location.search + '&petType_id=2'">
                            <img class="rig-img" src="https://www.hillspet.com/content/dam/cp-sites/hills/hills-pet/en_us/exported/cat-care/new-pet-parent/images/mother-cat-and-kitten-sleeping.jpg">
                            <span class="rig-overlay"></span>
                            <span class="rig-text">Cats</span>
                        </a>
                    </li>
                    <li>
                        <a class="rig-cell" onclick="window.location.href='/petpreference' + window.location.search + '&petType_id=3'">
                            <img class="rig-img" src="https://static.vets-now.com/uploads/2019/10/rabbit1.jpg">
                            <span class="rig-overlay"></span>
                            <span class="rig-text">Rabbit</span>
                        </a>
                    </li>
                    <li>
                        <a class="rig-cell" onclick="window.location.href='/petpreference' + window.location.search + '&petType_id=4'">
                            <img class="rig-img" src="https://cache.desktopnexus.com/thumbseg/2367/2367473-bigthumbnail.jpg">
                            <span class="rig-overlay"></span>
                            <span class="rig-text">Horses</span>
                        </a>
                    </li>
                    <li>
                        <a class="rig-cell"onclick="window.location.href='/petpreference' + window.location.search + '&petType_id=5'">
                            <img class="rig-img" src="https://media-cdn.tripadvisor.com/media/photo-s/09/13/00/dc/aran-islands-walks.jpg">
                            <span class="rig-overlay"></span>
                            <span class="rig-text">Donkeys</span>
                        </a>
                    </li>
                    <li>
                        <a class="rig-cell" onclick="window.location.href='/petpreference' + window.location.search + '&petType_id=6'">
                            <img class="rig-img" src="https://www.wallpapers13.com/wp-content/uploads/2015/12/Cute-Love-birds-90764-915x515.jpg">
                            <span class="rig-overlay"></span>
                            <span class="rig-text">Birds</span>
                        </a>
                    </li>
                </ul>
                <a class="btn btn-dark mt-3 mb-3" style="float:right" href="/home" role="button">Skip</a>
            </form:form>


        </div>
    </div>
</div>
<div class="container">
    <jsp:include page="footer.jsp" />

  </div>

<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
        crossorigin="anonymous"></script>

</body></html>