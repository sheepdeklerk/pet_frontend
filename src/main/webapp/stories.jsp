<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="ISO-8859-1">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
    integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">
  <link rel="stylesheet" href="/css/style.css">

  <title>Stories - Pet Tinder</title>
</head>

<body>
  <jsp:include page="header.jsp" />
	<div class="container border-bottom border-top pb-2 mb-3">
		 <h1 class="display-4 text-center">Success stories</h1>
	</div>
  <div class="container">
    <diV class="row">
      <div class="card-columns">
        <div class="card">
          <div class="card-body">
            <h5 class="card-title">Recently adopted a pet?</h5>
            <p class="card-text ">Send your adoption tales to <a href="mailto:stories@pettinder.co.za">
                stories@pettinder.co.za</a></p>
          </div>
        </div>
        <c:forEach items="${storyList}" var="storyList">
          <div class="card" >
              <!-- <div class="card-header">

              </div> -->
            <img class="card-img-top" src="${storyList.imageUrlOne}" alt="Card image cap">
            <div class="card-body">
              <h5 class="card-title">${storyList.title}</h5>
              <p class="card-text ">${storyList.story}</p>
            </div>
            <div class="card-footer">
              <small class="text-muted"> - ${storyList.author}</small>
            </div>
          </div>
        </c:forEach>
        
      </div>
    </diV>

  </div>
  <div class="container">
    <jsp:include page="footer.jsp" />

  </div>

  <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
    integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
    crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
    integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
    crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
    integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
    crossorigin="anonymous"></script>
</body>

</html>