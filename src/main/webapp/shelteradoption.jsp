<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<!doctype html>
<html lang="en">

<head>
	<!-- Required meta tags -->
	<meta charset="ISO-8859-1">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
		integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">
	<link rel="stylesheet" href="/css/style.css">

	<!-- Font Awesome -->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

	<title>Home - Pet Tinder</title>
</head>

<body>
	<jsp:include page="header.jsp" />
	<div class="container border-bottom border-top pb-2 mb-3">
		<h1 class="display-4 text-center">Pets at ${currentShelter.name}</h1>
	</div>
	<div class="container">
		<div class="row">

			<a href="/requests" class="btn btn-dark btn-lg btn-block">
				<i class="material-icons">email</i> Requests
			</a>
		</div>

	</div>
	<div class="container">
		<div class="row">
			<div class="col">
				<h1>Available Pets</h1>
				<div class="card-deck">
					<c:forEach items="${petList}" var="petList">
						<div class="card">
							<img class="card-img-top" src="${petList.imageUrlOne}" alt="Card image cap">
							<div class="card-body">
								<h5 class="card-title">${petList.petName}</h5>
								<p class="card-text ">${petList.story}</p>
							</div>
							<div class="card-footer">
								<small class="text-muted"> 5 people interested</small>

							</div>
						</div>
					</c:forEach>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row pt-2">
			<div class="col">
				<a class="btn-lg btn btn-pet btn-block" href="/petregistration">Add a new pet</a>
			</div>
		</div>
	</div>
	</div>
	<div class="container">
		<jsp:include page="footer.jsp" />
	
	  </div>

	<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
		integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
		crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
		integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
		crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
		integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
		crossorigin="anonymous"></script>
</body>

</html>